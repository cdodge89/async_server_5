var http = require("http");
var express = require("express");
var movies = require("./movies");
var logger = require("./logger");

var port = 8080;
var route = express.Router();
var app = express();

var server = app.listen(port, function(){
    console.log('listening at port ' + port);
});

